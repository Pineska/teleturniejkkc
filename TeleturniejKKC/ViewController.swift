//
//  ViewController.swift
//  TeleturniejKKC
//
//  Created by Tomasz Rejdych on 12/06/15.
//  Copyright (c) 2015 Tomasz Rejdych. All rights reserved.
//

import UIKit
import Foundation
import AVFoundation

    let maxTime = 4
class ViewController: UIViewController {
    var timer = NSTimer()
    var audioPlayer = AVAudioPlayer()
    var counter = maxTime
    @IBOutlet weak var startButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func startAction(sender: AnyObject) {
        if (sender.titleForState(UIControlState.Normal) != "Start") {
            restartButtonText()
        }else {
            sender.setTitle("5", forState: UIControlState.Normal)
            timer = NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("updateTimer"), userInfo: nil, repeats: true)
        }
    }


    @IBAction func goodAnswer(sender: AnyObject) {
        playSound("true")
        restartButtonText()
    }
    
    
    @IBAction func wrongAnswer(sender: AnyObject) {
        playSound("false")
        restartButtonText()
    }
 
    @IBAction func introSound(sender: AnyObject) {
        playSound("intro")
    }
    
    func playSound(name: String) {
        
        let soundURL = NSBundle.mainBundle().URLForResource(name, withExtension: "caf")
        let dispatchQueue =
        dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)
        
        dispatch_async(dispatchQueue, {[weak self] in
            let mainBundle = NSBundle.mainBundle()
            
            /* Find the location of our file to feed to the audio player */
            let filePath = mainBundle.pathForResource(name, ofType:"mp3")
            
            if let path = filePath{
                let fileData = NSData(contentsOfFile: path)
                
                var error:NSError?
                
                /* Start the audio player */
                self!.audioPlayer = AVAudioPlayer(data: fileData, error: &error)
                
                /* Did we get an instance of AVAudioPlayer? */
                if let player = self?.audioPlayer{
                    /* Set the delegate and start playing */
//                    player.delegate = self
                    if player.prepareToPlay() && player.play(){
                        /* Successfully started playing */
                    } else {
                        /* Failed to play */
                    }
                } else {
                    /* Failed to instantiate AVAudioPlayer */
                }
            }
            
            })
    }
    
    func restartButtonText() {
        startButton.setTitle("Start", forState: UIControlState.Normal)
        timer.invalidate()
        counter = maxTime
    }
    func updateTimer() {
//        static Integer i = 5
        var title :String! = String(format: "%d", counter--)
        if (counter == -1) {
            restartButtonText()
        }else {
            
            startButton.setTitle(title, forState: UIControlState.Normal)
        }
    }
}

